module TopLevelTb();


parameter Halfcycle = 10;

localparam Cycle = 2*Halfcycle;
localparam Cycle25 = 2*Cycle;
localparam Cycle125 = 2*Cycle25;

reg Clock, Reset;

initial Clock = 0;
always #(Halfcycle) Clock = ~Clock;

reg [3:0] Switch;
reg rx;
wire tx;

TopLevel tl(
    .clock_50MHz(Clock),
    .Switch(Switch),
    .UART_Rx(rx),

    .UART_Tx(tx)
);

initial begin

    Switch[0] = 1;
    #(Cycle);

    Switch[0] = 0;
    repeat(300000) begin
        #(Cycle125);
        if (tl.ctrl.state == 2'b11)
            $display("%d",$signed(tl.ctrl.temp_reg));
    end

    $finish();
end

endmodule
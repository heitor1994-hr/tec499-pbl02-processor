`timescale 1ns / 1ps

module AlgPrimesTb ();

    parameter Halfcycle = 70;

    localparam Cycle = 2*Halfcycle;

    reg Clock, Reset;

    initial Clock = 0;
    always #(Halfcycle) Clock = ~Clock;

    wire [31:0] address;
    wire writeEnable;
    wire [31:0] writeData;
    wire [31:0] readData;

<<<<<<< HEAD:sim/tb/AlgPrimesTb.v
    RAM #(.FILE_IN("/home/heitor/Dropbox/SD/assembler/algs/primes.mc")) ram  (
=======
    RAM #(.FILE_IN("/home/rcluan/Luan/tec499/Processor/algs/primes.mc")) ram  (
>>>>>>> 75886144c828448749834e1a2f5ce16b22edad6b:tb/AlgPrimesTb.v
        .Clock(Clock),
        .Address(address[15:2]),
        .MemWrite(writeEnable),
        .WriteData(writeData),
        .ReadData(readData)
    );

    Processor cpu (
        .Clock(Clock),
        .Reset(Reset),
        .Mem_DataIn(readData),
        .Mem_Address(address),
        .Mem_WriteEnable(writeEnable),
        .Mem_DataOut(writeData)
    );

    integer i;

    localparam wordsInMemoryFile = 16384;
    localparam wordsInRegFile = 32;

    reg [31:0] memSimContent [0:wordsInMemoryFile-1];
    reg [31:0] regfileSimContent [0:wordsInRegFile-1];

    task checkMemory;
        begin
            for (i = 0; i < wordsInMemoryFile; i= i+1) begin
                if (ram.memory[i] != memSimContent[i]) begin
                    $display("Memory Address: 0x%h RAM: %b Simulated Result: %b",i,ram.memory[i],memSimContent[i]);
                    $display("FAILED.");
                    $finish();
                end
            end
            $display("Memory: PASSED!");
        end
    endtask

    task checkRegisterFile;
        begin /*
            for (i = 0; i < wordsInRegFile; i= i+1) begin
                if (cpu.idblock.registerFile.registers[i] != regfileSimContent[i]) begin
                    $display("Register File Address: 0x%h RAM: %b Simulated Result: %b",i,cpu.idblock.registerFile.registers[i],regfileSimContent[i]);
                    $display("FAILED.");
                    $finish();
                end
            end*/
            $display("Register File: PASSED!");
        end
    endtask

    initial begin
<<<<<<< HEAD:sim/tb/AlgPrimesTb.v
      $readmemb("/home/heitor/Dropbox/SD/processor/sim/tests/primes_mem_out.bin",memSimContent);
      $readmemb("/home/heitor/Dropbox/SD/processor/sim/tests/primes_regs_out.bin",regfileSimContent);
=======
      $readmemb("/home/rcluan/Luan/tec499/Processor/tests/primes_mem_out.bin",memSimContent);
      $readmemb("/home/rcluan/Luan/tec499/Processor/tests/primes_regs_out.bin",regfileSimContent);
>>>>>>> 75886144c828448749834e1a2f5ce16b22edad6b:tb/AlgPrimesTb.v
      Reset = 1;
      #(Cycle);
      Reset = 0;
      
      while (address != 32'h3ffc) begin
        #(Cycle);
      end
      
      checkMemory();
      //checkRegisterFile();
      $finish();
    end
endmodule

